export function loadingIndicatorHookRunBlock($transitions, LoadingIndicatorService) {
  $transitions.onStart( { /* match anything */ }, LoadingIndicatorService.showLoadingIndicator);
  $transitions.onFinish( { }, LoadingIndicatorService.hideLoadingIndicator);
}
loadingIndicatorHookRunBlock.$inject = ['$transitions', 'LoadingIndicatorService'];
