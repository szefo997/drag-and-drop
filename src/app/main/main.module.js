import {tsApp} from "./component/App.component";
import {tsDragAndDrop} from "./component/DragAndDrop.component";
import {DIRECTIVES_MODULE} from "../directives/directives.module";
import {
    appState,
    homeState,
    notFoundState
} from "./app.states";
import {tsNotFound} from "./component/404.component";
import {tsHeaderComponent} from "./component/Header.components";
import DragAndDropService from "./service/DragAndDrop.service";
import "angular-ui-tree";
import "angular-ui-bootstrap";
import {tsDragAndDropForm} from "./component/DragAndDropForm.component";

export const MAIN_MODULE = angular.module('main', ['ui.bootstrap', 'ui.tree',
    DIRECTIVES_MODULE.name
]);

MAIN_MODULE.config(['$uiRouterProvider', $uiRouter => {
    $uiRouter.trace.enable(1);

    const $urlService = $uiRouter.urlService;
    $urlService.rules.otherwise({state: '404'});

    const $stateRegistry = $uiRouter.stateRegistry;
    $stateRegistry.register(appState);
    $stateRegistry.register(homeState);
    $stateRegistry.register(notFoundState);
}]);

MAIN_MODULE.component('tsApp', tsApp);
MAIN_MODULE.component('tsNotFound', tsNotFound);
MAIN_MODULE.component('tsDragAndDrop', tsDragAndDrop);
MAIN_MODULE.component('tsDragAndDropForm', tsDragAndDropForm);
MAIN_MODULE.component('tsHeaderComponent', tsHeaderComponent);
MAIN_MODULE.service('DragAndDropService', DragAndDropService);

