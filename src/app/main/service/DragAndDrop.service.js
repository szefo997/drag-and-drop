export default class DragAndDropService {

    constructor(UtilsService) {
        this.UtilsService = UtilsService;
    }

    static getParentNode(scope) {
        if (scope.depth() === 0) return null;
        const node = scope.$parent.node;
        if (angular.isDefined(node) && scope.depth() === 1)
            return node;
        else
            return DragAndDropService.getParentNode(scope.$parent);
    }


    static isAbove(parentNode, source, callback) {
        if (parentNode.id === source.id) {
            callback(parentNode, source);
            return;
        }
        for (const c of parentNode.nodes) {
            if (c.id === source.id) {
                if (angular.isFunction(callback)) callback(c, source);
                break;
            }
            DragAndDropService.isAbove(c, source, callback);
        }
    }


    static traverse(source, callback) {
        console.warn('traverse', source);
        for (const c of source.nodes) {
            if (angular.isFunction(callback)) callback(c);
            DragAndDropService.traverse(c, callback);
        }
    }

    static download(content, fileName, contentType) {
        const a = document.createElement("a");
        const file = new Blob([content], {type: contentType});
        a.href = URL.createObjectURL(file);
        a.download = fileName;
        a.click();
    }

    getExampleData(lan) {
        if (lan === 'en')
            return this.UtilsService.get('data_EN.json');
        else
            return this.UtilsService.get('data_PL.json');
    }

}

DragAndDropService.$inject = ['UtilsService'];

