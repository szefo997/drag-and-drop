export default class UtilsService {

    constructor($http, $q, $log) {
        this.$http = $http;
        this.$q = $q;
        this.$log = $log;
    }

    get(url, param = null) {
        const deferred = this.$q.defer();
        this.$http.get(url, param).then(response => {
            deferred.resolve(response.data);
        }, errResponse => {
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }

    post(url, requestBody, config) {
        const deferred = this.$q.defer();
        this.$http.post(url, requestBody, config)
            .then(response => {
                deferred.resolve(response.data);
            }, errResponse => {
                deferred.reject(errResponse);
            });
        return deferred.promise;
    }

    put(url, params) {
        const deferred = this.$q.defer();
        this.$http.put(url, params).then(response => {
            deferred.resolve(response.data);
        }, errResponse => {
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }

    patch(url, params) {
        const deferred = this.$q.defer();
        this.$http.patch(url, params).then(response => {
            deferred.resolve(response.data);
        }, errResponse => {
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }

    del(url, params) {
        const deferred = this.$q.defer();
        this.$http.delete(url, params)
            .then(response => {
                deferred.resolve(response.data);
            }, errResponse => {
                deferred.reject(errResponse);
            });
        return deferred.promise;
    }

    checkAmountWatchers() {
        const root = angular.element(document.getElementsByTagName('html'));
        const watchers = [];
        const watchersWithoutDuplicates = [];
        const f = element => {
            angular.forEach(['$scope', '$isolateScope'], scopeProperty => {
                if (element.data() && element.data().hasOwnProperty(scopeProperty)) {
                    angular.forEach(element.data()[scopeProperty].$$watchers, watcher => {
                        watchers.push(watcher);
                    });
                }
            });
            angular.forEach(element.children(), childElement => {
                f(angular.element(childElement));
            });
        };

        f(root);

        angular.forEach(watchers, item => {
            if (watchersWithoutDuplicates.indexOf(item) < 0) {
                watchersWithoutDuplicates.push(item);
            }
        });

        this.$log.log('watchers.length = ' + watchersWithoutDuplicates.length);
    }

}


UtilsService.$inject = ['$http', '$q', '$log'];