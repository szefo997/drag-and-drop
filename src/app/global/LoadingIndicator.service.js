export class LoadingIndicatorService {
    constructor($rootScope, $timeout) {

        this.showLoadingIndicator = () => {
        };

        this.hideLoadingIndicator = () => {
            if (!$rootScope.loaded) {
                $timeout(() => {
                    $rootScope.loaded = true;
                }, 500);
            }
        };
    }
}

LoadingIndicatorService.$inject = ['$rootScope', '$timeout'];
