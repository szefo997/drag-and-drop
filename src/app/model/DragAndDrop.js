export default class DragAndDrop {

    constructor(id,
                code,
                description,
                name,
                createdOn,
                type) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.name = name;
        this.createdOn = createdOn;
        this.type = type;
        this.nodes = [];
    }

}

export const TYPE = Object.freeze({
    LEFT: "left",
    RIGHT: "right"
});

