export default class HeaderController {

    constructor($rootScope, $translate) {
        this.$rootScope = $rootScope;
        this.$translate = $translate;
    }

    $onInit() {
        this.$rootScope.$on('$translateChangeSuccess', (event, data) => {
            this.$rootScope.lang = data.language;
        });
    }

    changeLanguage(langKey) {
        this.$translate.use(langKey);
    }

}

HeaderController.$inject = ['$rootScope', '$translate'];

