import updateTitle from "./directives/UpdateTitle";

export const DIRECTIVES_MODULE = angular.module('directives', []);

DIRECTIVES_MODULE.directive('updateTitle', ['$transitions', ($transitions) => new updateTitle($transitions)]);
