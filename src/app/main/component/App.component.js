import AppController from "../controller/App.controller";
import template from './app.html';

export const tsApp = {
    controller: AppController,
    template: template
};
