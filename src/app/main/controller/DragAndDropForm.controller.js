export default class DragAndDropFormController {

    constructor($scope) {
        this.$scope = $scope;

        this.resolve = undefined;
        this.close = undefined;
        this.dsimiss = undefined;
    }

    $onInit() {
        this.node = this.resolve.node;
    }

    ok() {
        this.submitted = true;
        if (this.$scope.node_form.$valid)
            this.close({$value: this.node});
    }

    cancel() {this.dismiss({$value: 'cancel'});}

}

DragAndDropFormController.$inject = ['$scope'];
