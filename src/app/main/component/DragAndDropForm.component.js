import template from './dragAndDropForm.html';
import './dragAndDrop.scss';
import DragAndDropFormController from "../controller/DragAndDropForm.controller";

export const tsDragAndDropForm = {
    template,
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: DragAndDropFormController
};
