export const tsNotFound = {
    template: `
        <section class="jumbotron text-center">
            <div class="container">
                <div class="page-not-found-modal">
                    <h1>404 Error</h1>
                    <p>Sorry, that page doesn't exist. <a ui-sref="home">Go to Home Page.</a></p>
                </div>
            </div>
        </section>
`
};
