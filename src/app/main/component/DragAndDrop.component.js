import DragAndDropController from "../controller/DragAndDrop.controller";
import template from './dragAndDrop.html';
import './dragAndDrop.scss';

export const tsDragAndDrop = {
    template,
    controller: DragAndDropController
};
