import HeaderController from "../controller/Header.controller";
import template from './header.html';

export const tsHeaderComponent = {
    template: template,
    controller: HeaderController
};
