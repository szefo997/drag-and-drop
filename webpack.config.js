const jquery = require("jquery");
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const ENV = process.env.npm_lifecycle_event;
const isTest = ENV === 'test' || ENV === 'test-watch';
const isProd = ENV === 'build';

module.exports = function makeWebpackConfig() {

    const config = {};

    config.entry = isTest ? void 0 : {
        app: './src/app/bootstrap/ngmodule.js'
    };

    config.output = isTest ? {} : {
        path: __dirname + '/dist',
        filename: isProd ? '[name].[hash].js' : '[name].bundle.js',
    };

    if (isTest) {
        config.devtool = 'inline-source-map';
    }
    else if (isProd) {
        config.devtool = 'source-map';
    }
    else {
        config.devtool = 'eval-source-map';
    }

    config.module = {
        rules: [{
            test: require.resolve('jquery'),
            use: [{
                loader: 'expose-loader',
                options: 'jQuery'
            }, {
                loader: 'expose-loader',
                options: '$'
            }]
        }, {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.css$/,
            loader: isTest ? 'null-loader' : ExtractTextPlugin.extract({
                fallbackLoader: 'style-loader',
                loader: [
                    {loader: 'css-loader', query: {sourceMap: true}},
                    {loader: 'postcss-loader'}
                ],
            })
        }, {
            test: /\.(s*)css$/,
            use: ['style-loader', 'css-loader', 'sass-loader']
        }, {
            test: /\.(jpg|jpeg|png|gif)(\?.*)?$/,
            use: [
                {
                    loader: "file-loader",
                    options: {
                        name: "[name].[hash].[ext]",
                        publicPath: "../",
                        outputPath: "assets/image/",
                    },
                },
            ],
        }, {
            test: /\.(eot|otf|webp|ttf|woff|woff2|svg)(\?.*)?$/,
            use: [
                {
                    loader: "file-loader",
                    options: {
                        name: "[name].[hash].[ext]",
                        publicPath: "../",
                        outputPath: "assets/font/",
                    },
                },
            ],
        }, {
            test: /\.html$/,
            loader: 'raw-loader'
        }]
    };

    if (isTest) {
        config.module.rules.push({
            enforce: 'pre',
            test: /\.js$/,
            exclude: [
                /node_modules/,
                /\.spec\.js$/
            ],
            loader: 'istanbul-instrumenter-loader',
            query: {
                esModules: true
            }
        });
    }


    config.plugins = [
        new webpack.LoaderOptionsPlugin({
            test: /\.scss$/i,
            options: {
                postcss: {
                    plugins: [autoprefixer]
                }
            }
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery'
        })
    ];

    if (!isTest) {
        config.plugins.push(
            new HtmlWebpackPlugin({
                title: 'My App',
                template: './src/public/index.html'
            }),
            new HtmlWebpackExternalsPlugin({
                externals: [
                    {
                        module: 'jquery',
                        entry: 'dist/jquery.min.js',
                        global: 'jQuery',
                    },
                ],
            }),
            new ExtractTextPlugin({filename: 'css/[name].css', disable: !isProd, allChunks: true})
        );
    }

    if (isProd) {
        config.plugins.push(
            new webpack.NoErrorsPlugin(),
            new CopyWebpackPlugin([{
                from: __dirname + '/src/public'
            }])
        );
    }

    config.devServer = {
        contentBase: './src/public',
        // port: 4300
        stats: 'minimal',
        host: 'localhost'
    };

    return config;
}();
