export default class updateTitle {

    constructor($transitions) {
        this.restrict = 'A';
        this.$transitions = $transitions;
    }

    link(scope, element) {
        this.$transitions.onSuccess({}, (trans) => {
            const stateTo = trans.$to();
            if (stateTo.data && stateTo.data.pageTitle)
                element.text(stateTo.data.pageTitle);
        });
    }
}

