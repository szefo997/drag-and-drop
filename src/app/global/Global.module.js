import {loadingIndicatorHookRunBlock} from "./LoadingIndicator.hook";
import {LoadingIndicatorService} from "./LoadingIndicator.service";

export const GLOBAL_MODULE = angular.module('global', []);

GLOBAL_MODULE.service('LoadingIndicatorService', LoadingIndicatorService);
GLOBAL_MODULE.run(loadingIndicatorHookRunBlock);