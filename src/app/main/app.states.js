export const appState = {
    name: 'app',
    component: 'tsApp'
};

export const homeState = {
    parent: 'app',
    name: 'home',
    url: '/',
    component: 'tsDragAndDrop',
    data: {pageTitle: 'Strona startowa'}
};

export const notFoundState = {
    parent: 'app',
    name: '404',
    url: '/404',
    component: 'tsNotFound',
    data: {pageTitle: '404'}
};


returnTo.$inject = ['$transition$'];

function returnTo($transition$) {
    if ($transition$.redirectedFrom() !== null)
        return $transition$.redirectedFrom().targetState();

    const $state = $transition$.router.stateService;

    if ($transition$.from().name !== '')
        return $state.target($transition$.from(), $transition$.params("from"));

    return $state.target('home');
}

