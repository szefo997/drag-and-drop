import DragAndDropService from "../service/DragAndDrop.service";
import {TYPE} from "../../model/DragAndDrop";

export default class DragAndDropController {

    constructor($http, $translate, $scope, $rootScope, $uibModal, $timeout, DragAndDropService, moment) {
        this.$http = $http;
        this.$translate = $translate;
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$uibModal = $uibModal;
        this.$timeout = $timeout;
        this.DragAndDropService = DragAndDropService;
        this.moment = moment;
    }

    $onInit() {
        this.initTreesOptions();
        this.getExampleData();
        this.$rootScope.$on('$translateChangeSuccess', (event, data) => {
            this.getExampleData();
        });
    }

    getExampleData() {
        this.isLoaded = false;
        this.DragAndDropService.getExampleData(this.$translate.use())
            .then(data => this.initData(data))
            .catch(response => {
                this.isLoaded = true;
                console.error(response);
            });
    }

    initTreesOptions() {
        this.treeOptions1 = {
            beforeDrop: e => {
                const parentNode = DragAndDropService.getParentNode(e.dest.nodesScope);
                const clone = e.source.nodeScope.$modelValue;

                if (parentNode === null)
                    return true;

                if (parentNode.id === clone.id)
                    return false;

                let found = false;
                DragAndDropService.traverse(clone, currentNode => {
                    DragAndDropService.isAbove(parentNode, currentNode, (parent, source) => {
                        if (parent.id === source.id) {
                            console.warn('found');
                            found = true;
                        }
                    });
                });

                return !found;
            }
        };

        this.treeOptions2 = {
            accept: () => {
                return false;
            },
            beforeDrop: e => {
                const parentNode = DragAndDropService.getParentNode(e.dest.nodesScope);
                const clone = e.source.cloneModel;

                if (parentNode === null)
                    return true;

                if (parentNode.id === clone.id)
                    return false;

                let found = false;
                DragAndDropService.isAbove(parentNode, clone, (parent, source) => {
                    if (parent.id === source.id) {
                        console.warn('found');
                        found = true;
                    }
                });

                return !found;
            }
        };
    }

    initData(data) {
        this.timeout = this.$timeout(() => {
            this.tree1 = data.filter(v => {
                v.nodes = [];
                return v.type === TYPE.LEFT;
            });

            this.tree2 = data.filter(v => {
                v.nodes = [];
                return v.type === TYPE.RIGHT;
            });
            this.isLoaded = true;
            this.timeout = undefined;
        }, 500);
    }

    $onDestroy() {
        if (angular.isDefined(this.timeout)) {
            this.timeout.cancel(this.timeout);
            this.timeout = undefined;
        }
    }

    remove(scope) {
        scope.remove();
    }

    toggle(scope) {
        scope.toggle();
    }

    edit(scope) {
        const modalInstance = this.$uibModal.open({
            animation: true,
            component: 'tsDragAndDropForm',
            resolve: {
                node: () => angular.isDefined(scope.$modelValue) ? angular.copy(scope.$modelValue)
                    : angular.copy(scope.$nodeScope.$modelValue)
            }
        });

        modalInstance.result.then(node => {
            if (angular.isObject(node)) {
                scope.$nodeScope.$modelValue.name = node.name;
                scope.$nodeScope.$modelValue.code = node.code;
                scope.$nodeScope.$modelValue.description = node.description;
            }
        }, () => {
            console.info('modal-component dismissed at: ' + new Date());
        });
    }

}

DragAndDropController.$inject = ['$http', '$translate', '$scope', '$rootScope', '$uibModal', '$timeout', 'DragAndDropService', 'moment'];
