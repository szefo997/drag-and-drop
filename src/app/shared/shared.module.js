import UtilsService from "./service/utils.service";

export const SHARED_MODULE = angular.module('shared', []);

SHARED_MODULE.service('UtilsService', UtilsService);

