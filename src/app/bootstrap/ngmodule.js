import * as angular from "angular";

import uiRouter from "@uirouter/angularjs";
// import ocLazyLoad from "oclazyload";
import {StickyStatesPlugin} from '@uirouter/sticky-states';
import {DSRPlugin} from '@uirouter/dsr';
import {SHARED_MODULE} from "../shared/shared.module";
import {MAIN_MODULE} from '../main/main.module';
import {GLOBAL_MODULE} from '../global/Global.module';
import {visualizer} from "@uirouter/visualizer";
import "angular-translate";
import "angular-moment";
import 'angular-sanitize'

import '../../style/style.scss';

export const app = angular.module('app', ['pascalprecht.translate', 'angularMoment', 'ngSanitize',
    uiRouter,
    // ocLazyLoad,
    SHARED_MODULE.name,
    MAIN_MODULE.name,
    GLOBAL_MODULE.name
]);

app.config(['$translateProvider', $translateProvider => {
    $translateProvider
        .translations('pl', {
            'BRAG_TASK': 'BRAG zadanie',
            'TREE_MODEL': 'Model drzewa',
            'TREE': 'Drzewo',
            "BUTTON_LANG_EN": "Angielski",
            "EDIT": "Edycja",
            "NAME": "Nazwa",
            "CODE": "Kod",
            "DESCRIPTION": "Opis",
            "CANCEL": "Anuluj",
            "SAVE": "Zapisz",
            "REQUIRED_FIELD": "To pole jest wymagane.",
            "BUTTON_LANG_PL": "Polski"
        })
        .translations('en', {
            'BRAG_TASK': 'BRAG task',
            'TREE_MODEL': 'Tree model',
            'TREE': 'Tree',
            "EDIT": "Edit",
            "NAME": "Name",
            "CODE": "Code",
            "DESCRIPTION": "Description",
            "CANCEL": "Cancel",
            "SAVE": "Save",
            "REQUIRED_FIELD": "This field is required.",
            "BUTTON_LANG_EN": "English",
            "BUTTON_LANG_PL": "Polish"
        })
        .preferredLanguage('en')
        .useSanitizeValueStrategy('sanitize');
}]);

app.run(['$rootScope', $rootScope => {
    $rootScope.lang = 'en';
}]);

app.config(['$uiRouterProvider', $uiRouter => {
    $uiRouter.plugin(StickyStatesPlugin);
    $uiRouter.plugin(DSRPlugin);
    visualizer($uiRouter);
}]);

app.config(['$locationProvider', $locationProvider => {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);